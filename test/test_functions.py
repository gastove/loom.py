import loom.functions as f
import loom.constants as c

from loom.runtime import NAMESPACES  # NOQA


class TestConsLists:
    def test_construction(self):
        first_val = 5
        second_val = c._NIL
        third_val = 6

        cons = f._CONS(first_val, second_val)

        # The new list should have length 2, everything in the correct
        # positions
        assert len(cons) == 2
        assert cons[0] == first_val
        assert cons[1] == second_val

        second_cons = f._CONS(third_val, cons)

        # This should not mutate the input cell
        assert cons != second_cons
        assert second_cons[0] == third_val

    def test_car(self):
        test_vals = [1, 2, 3]

        assert f._CAR(test_vals) == 1

    def test_cdr(self):
        test_vals = [1, 2, 3]

        assert f._CDR(test_vals) == [2, 3]


class TestInterns:
    def test_intern(self):
        f._INTERN('test_variable', 'test_value')
        global NAMESPACES
        assert NAMESPACES['user']['test_variable'] == 'test_value'


class TestEq:
    def test_basic_equality(self):
        a = 5
        b = 6
        c = 5

        assert f._EQ(a, b) is False
        assert f._EQ(a, c) is True


class TestCond:
    def test_basic_cond(self):
        clauses = [
            [False, 'this should not appear'],
            [True, 'result']
        ]

        assert f._COND(clauses) == 'result'
