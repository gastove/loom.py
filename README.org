* EH?

A python backend for [[https://gitlab.com/gastove/loom][Loom]]. What's Loom, you ask? Easy: not much of anything, yet
;P

* Steps


** DOING Go from absolutely nothing to any definition of something

For starters, let's see if we can't get from this:

#+BEGIN_SRC clojure
  (print "flarb")
#+END_SRC

To this:

#+BEGIN_SRC
['print' ['value': "flarb", 'type': 'string']]
#+END_SRC
