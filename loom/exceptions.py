class LoomUnrecognizedTokenException(Exception):
    pass


class LoomNotImplementedException(Exception):
    pass
