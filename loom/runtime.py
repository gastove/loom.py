from collections import defaultdict

ns_defaults = {'modules': {}}

# Oh god. State
NAMESPACES = defaultdict(lambda: ns_defaults)


def lookup(sym):
    global NAMESPACES
    return NAMESPACES['user'].get(sym)
