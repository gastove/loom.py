import importlib

from loom.runtime import NAMESPACES  # NOQA, because this jerk can't follow the usage of `global`
import loom.constants as c


def _CONS(f, s):
    """
    Constructs a new cons list; behavior depends on `s`:

    - If `s` is nil, return a new cons cell.
    - If `s` is not nil, prepend `f`

    It's worth admitting that that this is not truly a cons cell
    implementation. It's all python list ops. Whee!
    """
    if s:
        return [f] + s
    else:
        return [f, c._NIL]


def _CAR(cons):
    """Returns the first element of a `cons`"""
    return cons[0]


def _CDR(cons):
    """
    Returns everything after the first element of a cons. (In a real cons
    implementation, this would be the second thing in the cell.)
    """
    return cons[1:]


def _INTERN(sym, val, namespace='user'):
    global NAMESPACES
    NAMESPACES[namespace][sym] = val


def _EQ(a, b):
    return a == b


def _COND(clauses):
    """
    COND takes a series of forms, checking them in order. The first form in
    which the head element is truthy has its tail returned.
    """
    # check for a default condition -- if the final clause has length 1, it's
    # the default
    default = None
    if len(clauses[-1]) == 1:
        default = clauses.pop()

    for condition, candidate in clauses:
        if condition:
            return candidate

    return default


class LoomLambda:
    def __init__(self, params, body):
        """
        Represents a lambda function. Uses `eval` to create the final form, handles
        calling it.

        TODO: sanitize parameters before eval
        TODO: typecheck function calls
        """
        self.params = params
        self.body = body

        self._generate_fn()

    def __call__(self, *args):
        return self._call(*args)

    def _generate_fn(self):
        fn_tpl = """lambda {params}: {body} """
        p_str = ', '.join(self.params)

        fn = eval(fn_tpl.format(params=p_str, body=self.body))

        self.func = fn

    def _sanitize_arguments(self):
        pass

    def _typecheck_call(self, *args):
        return

    def _call(self, *args):
        self._typecheck_call(*args)
        return self.func(*args)


def _LAMBDA(params, fn_body):
    """
    Creates a new lambda function instance, currently implemented as an
    instance of `LoomLambda`
    """
    if not isinstance(params, list):
        params = [params]

    return LoomLambda(params, fn_body)


class LoomQuotedForm:
    def __init__(self, form):
        self.form = form


def _QUOTE(form):
    """
    _QUOTE stops a form from being evaluated.

    I'm going to need to understand what this means.
    """
    return LoomQuotedForm(form)


def _PRINT(*args):
    """
    Utility printing function; should probably, eventually, have a less, ah...
    naive implementation
    """
    print(*args)


def _IMPORT(module, alias, ns='user'):
    global NAMESPACES

    # We may, eventually, need to clear the import cache here.
    m = importlib.import_module(module)
    NAMESPACES[ns]['modules'][alias] = m


def _CALL_MODULE_FN(alias, fn, fn_args, ns='user'):
    global NAMESPACES
    mod = NAMESPACES[ns]['modules'][alias]

    fn = getattr(mod, fn)
    return fn(*fn_args)


FUNC_NAME_TO_IMPLS = {
    'print': _PRINT,
    'def': _INTERN,
    'cons': _CONS,
    'car': _CAR,
    'head': _CAR,
    'cdr': _CDR,
    'tail': _CDR,
    'lambda': _LAMBDA,
    'import': _IMPORT
}
