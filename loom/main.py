# Standard Lib
import json
import sys

# Third Party
from termcolor import colored

# Loom
from lex import lex
from eval import loom_eval


def main(argv):
    print('Okay!')
    to_parse = ' '.join(argv[1:])

    try:
        res = lex(to_parse)
    except Exception as e:
        print(colored('Well damn:', 'red'))
        raise(e)

    print('From: ')
    print('\t' + to_parse)
    print('I parsed:')
    print(json.dumps(res, indent=1))
    print()
    loom_eval(res)
    print(colored('Done.', 'green'))


if __name__ == '__main__':
    main(sys.argv)
