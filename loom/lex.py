from log import log
from exceptions import LoomUnrecognizedTokenException


# Useful Sets of Things
UPPER_ALPHAS = list('ABCDEFGHIJKLMNOPQRSTUVWXYZ')
LOWER_ALPHAS = list('abcdefghijklmnopqrstuvwxyz')
SYMBOLS = list('!?-_')
DIGITS = list('0123456789')

# Valid identifiers are made of:
IDENTIFIER_CHARS = UPPER_ALPHAS + LOWER_ALPHAS + DIGITS + SYMBOLS


def annotate(lexeme, _type):
    if _type == 'function':
        return lexeme
    else:
        return {
            'value': lexeme,
            'type': _type
        }


def lex(s):
    """
    Lexes Loom LISP strings into an intermediate representation of lists
    representing one of two cases:

    Case one: bare values.

    A value evaluates to itself:
    In  > "cat"
    Out > [cat]
    In  > 5
    Out > [5]

    Case two: function calls:
    In  > (print "cat)
    Out > [['print' 'cat']]
    """

    ast = []
    sexp = None
    in_string = False
    curr_item = ''
    lexeme = None
    curr_type = None

    for i in range(len(s)):
        tok = s[i]
        log.debug('Handling token: {}'.format(tok))

        # For now, we don't handle escaped quotes in strings. Eventually, we will.
        if in_string and tok != '"':
            log.debug('Building string by adding char: {}'.format(tok))
            curr_item += tok
            log.debug('String is now: {}'.format(curr_item))

        elif tok == ' ':  # At space, we move on to the next symbol to sort out
            log.debug('Appending item {} to sexp and resetting curr_item'.format(curr_item))
            sexp.append(annotate(curr_item, curr_type))
            curr_item = ''
            curr_type = None

        elif tok == '(':  # Open a new sexp
            log.debug('Creating a new symbolic expression')
            sexp = []
            curr_type = 'function'

        elif tok == ')':  # Close sexp
            log.debug('Closing symbolic expression')
            if curr_item:
                sexp.append(annotate(curr_item, curr_type))

            ast.append(sexp)
            curr_item = ''
            sexp = []

        # We're either entering or exiting a string; invert in_string
        elif tok == '"':
            log.debug('Now {} a string'.format(
                'entering' if not in_string else 'exiting')
            )
            in_string = not in_string

            if in_string:
                curr_type = 'string'
            else:
                sexp.append(annotate(curr_item, curr_type))
                curr_item = None
                curr_type = None

        # Token is constituent of a valid identifier
        elif tok in IDENTIFIER_CHARS:
            log.debug('Token {} is a valid identifier character')
            curr_item += tok
            if not curr_type:
                curr_type = 'symbol'

        # Token is part of a typed symbol
        elif tok in '->':
            pass

        # Token is part of a module or object access, eg `json/dumps`
        elif tok == '/':
            pass

        else:  # Unrecognized token
            raise(LoomUnrecognizedTokenException(
                'Could not recognize token: {}'.format(tok))
            )

    # A value evaluates to itself
    return ast or [curr_item]


_DATA_STRUCTURE_LITERAL_OPENERS = (
    '('                         # List
    '['                         # Vector
    '"'                         # String
)
_DATA_STRUCTURE_LITERAL_CLOSERS = (
    ')'                         # List
    ']'                         # Vector
    '"'                         # String
)
_DATA_STRUCTURE_LITERAL_OPEN_TO_CLOSE_MAP = {
    '(': ')',                        # List
    '[': ']',                        # Vector
    '"': '"'                         # String
}


def list_reader(s):
    pass


def vector_reader(s):
    pass


def string_reader(s):
    res = ''
    while True:
        nxt = s.pop()
        # Remember, input has been reversed to make a stack, for I am a clever
        # lad ;P
        res = nxt + res
        if nxt == '"':
            break

    return res


def dispatch_struct_reader(peek, working_str):
    if peek == '(':
        return list_reader(working_str)
    elif peek == '[':
        return vector_reader(working_str)
    elif peek == '"':
        return string_reader(working_str)
    else:
        pass


def alt_lex(s):
    # Here's a cool way to turn a string into something vaguely like a stack.
    working_str = [c for c in reversed(s)]

    while working_str:
        # Python doesn't give us [].peek(), so we'll peek our own damn selves
        next_character = working_str[-1]
        if next_character in _DATA_STRUCTURE_LITERAL_OPENERS:
            # dispatch_struct_reader(next_character, working_str)
            pass
        else:
            pass
