import logging
import sys

# Python Logging Ritual
log = logging.getLogger('Loom')
log.setLevel(logging.INFO)
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    '%(asctime)s - %(message)s'
)
ch.setFormatter(formatter)
log.addHandler(ch)
