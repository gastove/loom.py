from exceptions import LoomNotImplementedException
from functions import FUNC_NAME_TO_IMPLS
from log import log
from runtime import lookup


def resolve_fn(fn_name):
    fn = FUNC_NAME_TO_IMPLS.get(fn_name)
    if not fn:
        raise(LoomNotImplementedException(
            'Function named "{}" is not yet implemented'.format(fn_name))
        )

    return fn


def call_with_args(fn, args):
    log.debug('Parsing args: {}'.format(args))
    resolved_args = []

    for arg in args:
        if isinstance(arg, dict):

            if arg['type'] == 'symbol':
                if fn == FUNC_NAME_TO_IMPLS.get('def'):
                    # We're about to define this symbol, don't resolve it
                    resolved_args.append(arg['value'])
                else:
                    resolved_args.append(lookup(arg['value']))

            else:
                resolved_args.append(arg['value'])
        else:
            raise(RuntimeError('Somehow, this is not a dict: {}'.format(arg)))

    log.debug('Resolved the args to: {}'.format(args))
    fn(*resolved_args)


def loom_eval(ast):
    """
    eval or die trying ヽ(⌐■_■)ノ♪♬

    An AST should be a list in which every item is either a value (which
    evaluates to itself), or a list of the form [FUNC, ARG1, ARG2, ..., ARGN].
    An ARG may itself be another func (not yet implemented.)
    """
    print("=== OUTPUT ===")
    for form in ast:
        if not isinstance(form, list):
            # Just let me know it's working
            print(form)

        else:
            fn = resolve_fn(form[0])
            call_with_args(fn, form[1:])
